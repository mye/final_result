import filecmp
import shutil
import argparse
import os
import yaml
from time import time
import datetime
import stat


def pinger(ip_address):
    for i in range(1, 10):
        g = os.system('ping -c 1 -W 1 ' + str(ip_address) + '> /dev/null')  # for unix
        #g = os.system('ping -n 1 -w 1000 ' + str(ip_address) + '> /null')  # for win
        if g == 0:
            return True
        else:
            return False


def gen_addresses(count, group=1):
    ip_list = []
    for i in range(1, count + 1):
        ip_list.append('172.16.' + str(group) + '.' + str(i))
    return ip_list


def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--check', action='store_true', default=False)
    return parser


def logdir():
    if os.path.exists('log/copy_log_vc.yaml'):
        my_file = open("log/copy_log_vc.yaml", 'w')
        my_file.close()
    else:
        if os.path.exists('log/'):
            my_file = open("log/copy_log_vc.yaml", 'w')
            my_file.close()
        else:
            os.makedirs('log')
            my_file = open("log/copy_log_vc.yaml", 'w')
            my_file.close()


def yaml_dump(mydict_END_local):
    logdir()
    with open('log/copy_log_vc.yaml', 'w', encoding='utf-8') as fout:
        yaml.dump(mydict_END_local, fout, indent=4,
                  default_flow_style=False,
                  allow_unicode=True)


def approximate_size(size, a_kilobyte_is_1024_bytes=True):
    SUFFIXES = {1000: ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
                1024: ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']}

    if size < 0:
        raise ValueError('число должно быть неотрицательным')

    multiple = 1000 if a_kilobyte_is_1024_bytes else 1024
    # multiple = 1024 if a_kilobyte_is_1024_bytes else 1000
    for suffix in SUFFIXES[multiple]:
        size /= multiple
        if size < multiple:
            return '{0:.1f} {1}'.format(size, suffix)

    raise ValueError('число слишком большое')


def list_dir():
    dict_list_file = {}
    if os.path.exists('files/'):
        if os.path.exists('files/for_files/'):
            dict_list_file['files'] = os.listdir("files/for_files/")
            if os.path.exists('files/for_directory/'):
                dict_list_file['directory'] = os.listdir("files/for_directory/")
            else:
                os.makedirs('files/for_directory/')
        else:
            os.makedirs('files/for_files/')
    else:
        print(
            '\nРядом с файлом скрипта создана директория {}, в ней еще две: \n\tдля размещения файлов - {} \n\tи директорий - {}'.format(
                'files', 'for_files', 'for_directory.'))
        print(
            'Поместите фАЙЛЫ в дректорию files/for_files/ \nили ДИРЕКТОРИИ в директорию files/for_directory/ \nи перезапустите скрипт.')
        os.makedirs('files/for_files/')
        os.makedirs('files/for_directory/')

    return dict_list_file


def choose_way_vc():
    print('\nПамятка бойца вц: \nvc01a-111 vc01b-112 vc01c-113 vc01d-114 vc01e-115')
    number_vc = input('\nВведите номер вц: ')
    print('\nВыбор пути:')
    print('1 - VC {} путь - Users\student\Desktop'.format(number_vc))
    print('2 - VC {} путь - Users\student\Documents'.format(number_vc))
    print('\nДля классов где установленная win 10:')
    print('3 - VC {} путь - Users\student2\Desktop'.format(number_vc))
    print('4 - VC {} путь - Users\student2\Documents'.format(number_vc))
    print('5 - VC {} пустая переменная'.format(number_vc))

    remote_way_host = int(input('\nВведите номер соответствующий нужному пути: '))

    if remote_way_host == 1:
        local_way1 = '\\C$\\Users\\student\\Desktop'

    elif remote_way_host == 2:
        local_way1 = '\\C$\\Users\\student\\Documents'

    elif remote_way_host == 3:
        local_way1 = '\\C$\\Users\\student2\\Desktop'

    elif remote_way_host == 4:
        local_way1 = '\\C$\\Users\\student2\\Documents'

    elif remote_way_host == 5:
        # pass
        #local_way1 = '\\C$\\Program Files (x86)'
        #local_way1 = '\\C$\\Users\Public\Desktop'
        #local_way1 = '\\C$\\Intes\\QDPro'
        #local_way1 = '\\C$\\Users\\student\\Documents\\!VM'
        #local_way1 = '\\C$\\Program Files (x86)\\Cisco Packet Tracer 5.3.3\\languages'
        #local_way1 = '\\C$\\Users\\Public\\Desktop'
        local_way1 = '\\C$'

    else:
        print('remote_way_host != 1,2,3,4')

    return number_vc, local_way1


def chmod_R(tree_way):
    os.chmod(tree_way, stat.S_IWRITE)
    tree = os.walk(tree_way)
    for root, dirs, files in tree:
        for name in files:
            os.chmod(os.path.join(root, name), stat.S_IWRITE) # смена прав для файла убирает ro
            #print(os.path.join(root, name))
        for name in dirs:
            os.chmod(os.path.join(root, name), stat.S_IWRITE) # смена прав для директории убирает ro
            #print(os.path.join(root, name))


def del_file_vc():
    print('Памятка бойца вц: \nvc01a-111 vc01b-112 vc01c-113 vc01d-114 vc01e-115')
    print('\nВыбрана функция удаления файлов.')
    if os.path.exists('files/for_files/'):
        if os.listdir("files/for_files/") == []:
            print(
                '\nДиректория для ФАЙЛОВ пуста, \nпоместите ФАЙЛЫ для удаления в директорию files/for_files/ \nи перезапустите скрипт.')
        else:
            print('\nОбразцы для удаления::\n{}'.format(os.listdir("files/for_files/")))
            yaml_dump(list_dir())
            number_vc, local_way1 = choose_way_vc()
            tic = time()
            dict_hosts = {}
            mydict_END = {}
            now_time = datetime.datetime.now()
            for address in gen_addresses(26, number_vc):
                if pinger(address):
                    print('\nhost {}{}'.format(address, ' online'))
                    dict_hosts[address] = 'online'
                    with open("log/copy_log_vc.yaml", 'r') as stream:
                        data_loaded = yaml.load(stream)
                    for item in data_loaded['files']:
                        top_way = str('\\\\' + address + local_way1 + '\\' + item)
                        way_without_item = str('\\\\' + address + local_way1 + '\\')
                        local_item_way = str('files/for_files/' + item)
                        if os.path.exists(top_way):
                            if os.path.isfile(top_way):
                                print('Файл -> {}\nРазмером -> {}\nПо адресу -> {}\nСуществует, удаляем.'.format(item,
                                                                                                                  approximate_size(
                                                                                                                      os.path.getsize(
                                                                                                                          local_item_way)),
                                                                                                                  top_way))
                                try:
                                    chmod_R(top_way)
                                    #os.chmod(top_way, stat.S_IWRITE)
                                except:
                                    pass
                                os.remove(top_way)
                            else:
                                print('Файл {} {}, \nОтсутствует.\n'.format(
                                    approximate_size(os.path.getsize(local_item_way)), top_way))
                        else:
                            print('Файл -> {}\nразмером -> {}\nпо адресу -> {}\nОтсутствует.'.format(item,
                                                                                                     approximate_size(
                                                                                                         os.path.getsize(
                                                                                                             local_item_way)),
                                                                                                     top_way))
                else:
                    dict_hosts[address] = 'offline!'
                    print('host {}{}'.format(address, ' offline!'))
            mydict_END['1.Start-up time'] = now_time.strftime("%d.%m.%Y %H:%M")
            mydict_END['2.HOSTS Where the files were deleting'] = dict_hosts
            toc = time()
            print('Passed the time {}'.format(toc - tic))
            mydict_END['3.Passed the time'] = toc - tic
            mydict_END['4.done'] = '...........................................'
            return mydict_END
    else:
        list_dir()


def del_directory_vc():
    print('Памятка бойца вц: \nvc01a-111 vc01b-112 vc01c-113 vc01d-114 vc01e-115')
    print('\nВыбрана функция удаления директорий.')
    if os.path.exists('files/for_directory/'):
        if os.listdir("files/for_directory/") == []:
            print(
                '\nДиректория для директорий пуста, \nпоместите ДИРЕКТОРИИ для удаления в директорию files/for_directory/ \nи перезапустите скрипт.')
        else:
            print('\nОбразцы для удаления:\n{}'.format(os.listdir("files/for_directory/")))
            yaml_dump(list_dir())
            number_vc, local_way1 = choose_way_vc()
            tic = time()
            dict_hosts = {}
            mydict_END = {}
            now_time = datetime.datetime.now()
            for address in gen_addresses(26, number_vc):
                if pinger(address):
                    print('\nhost {}{}'.format(address, ' online'))
                    dict_hosts[address] = 'online'
                    with open("log/copy_log_vc.yaml", 'r') as stream:
                        data_loaded = yaml.load(stream)
                    for item in data_loaded['directory']:
                        top_way = str('\\\\' + address + local_way1 + '\\' + item)
                        try:
                            chmod_R(top_way)
                        except:
                            pass
                        way_without_item = str('\\\\' + address + local_way1 + '\\')
                        local_item_way = str('files/for_directory/' + item)
                        if os.path.exists(top_way):
                            if os.path.isdir(top_way):
                                print('Директория -> {}\nразмером -> {}\nпо адресу -> {}\nCуществует, удаляем.'.format(
                                    item, approximate_size(
                                        os.path.getsize(local_item_way)), top_way))
                                shutil.rmtree(top_way)
                            else:
                                print('Директория -> {}\nразмером -> {}\nпо адресу -> {}\nОтсутствует'.format(item,
                                                                                                             approximate_size(
                                                                                                                 os.path.getsize(
                                                                                                                     local_item_way)),
                                                                                                             top_way))
                        else:
                            print('Директория -> {}\nразмером -> {}\nпо адресу -> {}\nОтсутствует'.format(item,
                                                                                                         approximate_size(
                                                                                                             os.path.getsize(
                                                                                                                 local_item_way)),
                                                                                                         top_way))
                else:
                    dict_hosts[address] = 'offline!'
                    print('host {}{}'.format(address, ' offline!'))

            mydict_END['1.Start-up time'] = now_time.strftime("%d.%m.%Y %H:%M")
            mydict_END['2.HOSTS Where the directory were deleting'] = dict_hosts
            toc = time()
            print('Passed the time {}'.format(toc - tic))
            mydict_END['3.Passed the time'] = toc - tic
            mydict_END['4.done'] = '...........................................'
            return mydict_END
    else:
        list_dir()


def copy_file_vc():
    print('Памятка бойца вц: \nvc01a-111 vc01b-112 vc01c-113 vc01d-114 vc01e-115')
    print('\nВыбрана функция копирования ФАЙЛОВ.')
    if os.path.exists('files/for_files/'):
        if os.listdir("files/for_files/") == []:
            print(
                '\nДиректория для ФАЙЛОВ пуста, \nпоместите ФАЙЛЫ для копирования в директорию files/for_files/ \nи перезапустите скрипт.')
        else:
            print('\nВ директории для копирования ФАЙЛОВ находяться:\n{}'.format(os.listdir("files/for_files/")))
            yaml_dump(list_dir())
            number_vc, local_way1 = choose_way_vc()
            tic = time()
            dict_hosts = {}
            dict_log_procedure = {}
            mydict_END = {}
            now_time = datetime.datetime.now()
            for address in gen_addresses(26, number_vc):
                if pinger(address):
                    print('host {}{}'.format(address, ' online'))
                    dict_hosts[address] = 'online'
                    with open("log/copy_log_vc.yaml", 'r') as stream:
                        data_loaded = yaml.load(stream)
                    for item in data_loaded['files']:
                        top_way = str('\\\\' + address + local_way1 + '\\' + item)
                        way_without_item = str('\\\\' + address + local_way1 + '\\')
                        local_item_way = str('files/for_files/' + item)
                        if os.path.exists(top_way):
                            if os.path.isfile(top_way):
                                print(
                                    '\nФайл -> {}\nразмером -> {}\nпо адресу -> {}\nСуществует, начинаем сравнение...'.format(
                                        item, approximate_size(os.path.getsize(local_item_way)), top_way))
                                eq = filecmp.cmp(local_item_way, top_way)
                                if eq:
                                    print('\nРезультат сравнения файла {} : Идентичен исходнику, пропускаем...\n'.format(
                                        item))
                                else:
                                    print('файл', top_way, '\nНе идентичен исходнику, перезаписываем...\n')
                                    shutil.copyfile(local_item_way, top_way)
                            else:
                                print('файл {} {}, \nОтсутствует, начинаем копирование...\n'.format(
                                    approximate_size(os.path.getsize(local_item_way)), top_way))
                                shutil.copyfile(local_item_way, top_way)
                        else:
                            print(
                                '\nФайл -> {}\nразмером -> {}\nпо адресу -> {}\nотсутствует, начинаем копирование...'.format(
                                    item, approximate_size(os.path.getsize(local_item_way)), top_way))
                            try:
                                shutil.copyfile(local_item_way, top_way)
                            except FileNotFoundError:
                                print('\n[Error] хост {} - онлайн\nОтказ доступа по адресу {}'.format(address,
                                                                                                    way_without_item))
                                error_mess = str('[Error] хост {} - онлайн, отказ доступа по адресу {}'.format(address,
                                                                                                               way_without_item))
                                dict_log_procedure['Error'] = error_mess
                            except OSError:
                                print('\n[Error] хост {} - онлайн\nОтказ доступа по адресу {}'.format(address,
                                                                                                    way_without_item))
                                error_mess = str('[Error] хост {} - онлайн, отказ доступа по адресу {}'.format(address,
                                                                                                               way_without_item))
                                dict_log_procedure['Error'] = error_mess
                else:
                    dict_hosts[address] = 'offline!'
                    print('host {}{}'.format(address, ' offline!'))
            mydict_END['1.Start-up time'] = now_time.strftime("%d.%m.%Y %H:%M")
            mydict_END['2.HOSTS Where the files were copied'] = dict_hosts
            mydict_END['3.ERRORS'] = dict_log_procedure
            toc = time()
            print('Passed the time {}'.format(toc - tic))
            mydict_END['Passed the time'] = toc - tic
            mydict_END['done'] = '...........................................'
            return mydict_END
    else:
        list_dir()


def copy_directory_vc():
    print('Памятка бойца вц: \nvc01a-111 vc01b-112 vc01c-113 vc01d-114 vc01e-115')
    print('\nВыбрана функция копирования ДИРЕКТОРИЙ.')
    if os.path.exists('files/for_directory/'):
        if os.listdir("files/for_directory/") == []:
            print(
                '\nДиректория для директорий пуста, \nпоместите ДИРЕКТОРИИ для копирования в директорию files/for_directory/ \nи перезапустите скрипт.')
        else:
            print(
                '\nВ директории для копирования ДИРЕКТОРИЙ находяться:\n{}'.format(os.listdir("files/for_directory/")))
            yaml_dump(list_dir())
            number_vc, local_way1 = choose_way_vc()
            tic = time()
            dict_hosts = {}
            dict_log_procedure = {}
            mydict_END = {}
            now_time = datetime.datetime.now()
            for address in gen_addresses(26, number_vc):
                if pinger(address):
                    print('\nhost {}{}'.format(address, ' online'))
                    dict_hosts[address] = 'online'
                    with open("log/copy_log_vc.yaml", 'r') as stream:
                        data_loaded = yaml.load(stream)
                    for item in data_loaded['directory']:
                        top_way = str('\\\\' + address + local_way1 + '\\' + item)
                        way_without_item = str('\\\\' + address + local_way1 + '\\')
                        local_item_way = str('files/for_directory/' + item)
                        if os.path.exists(top_way):
                            if os.path.isdir(top_way):
                                print(
                                    'Директория\n{} \nСуществует, сравнение на стадии разработки --- пропускаем...'.format(
                                        top_way))
                            else:
                                print('Директория\n{} \nОтсутствует, начинаем копирование...\n'.format(top_way))
                                shutil.copytree(local_item_way, top_way)
                        else:
                            print('Директория {}\nпо адресу {}\nОтсутствует, начинаем копирование...'.format(item,
                                                                                                             way_without_item))
                            try:
                                shutil.copytree(local_item_way, top_way)
                            except FileNotFoundError:
                                print('\n[Error] хост {} - онлайн\nОтказ доступа по адресу {}'.format(address,
                                                                                                    way_without_item))
                                error_mess = str('[Error] хост {} - онлайн, отказ доступа по адресу {}'.format(address,
                                                                                                               way_without_item))
                                dict_log_procedure['Error'] = error_mess
                            except OSError:
                                print('\n[Error] хост {} - онлайн\nОтказ доступа по адресу {}'.format(address,
                                                                                                    way_without_item))
                                error_mess = str('[Error] хост {} - онлайн, отказ доступа по адресу {}'.format(address,
                                                                                                               way_without_item))
                                dict_log_procedure['Error'] = error_mess

                else:
                    dict_hosts[address] = 'offline!'
                    print('host {}{}'.format(address, ' offline!'))

            mydict_END['1.Start-up time'] = now_time.strftime("%d.%m.%Y %H:%M")
            mydict_END['2.HOSTS Where the directory were copied'] = dict_hosts
            mydict_END['3.ERRORS'] = dict_log_procedure
            toc = time()
            print('Passed the time {}'.format(toc - tic))
            mydict_END['4.Passed the time'] = toc - tic
            mydict_END['5.done'] = '...........................................'
            return mydict_END
    else:
        list_dir()


def restart_vc():
    print('Памятка бойца вц: \nvc01a-111 vc01b-112 vc01c-113 vc01d-114 vc01e-115')
    number_vc = input('\nВведите номер вц: ')
    tic = time()
    dict_hosts = {}
    now_time = datetime.datetime.now()
    for address in gen_addresses(26, number_vc):
        if pinger(address):
            dict_hosts[address] = 'online --> Command send to reboot'
            # os.system("shutdown " "/r " "/t 0 " "/m " + address + "> nul")
            os.system("shutdown /r /t 0 /m " + address + "> nul")
            print('host {}{}'.format(address, ' online --> Command send to reboot'))
        else:
            dict_hosts[address] = 'offline!'
            print('host {}{}'.format(address, ' offline!'))

            mydict_END = {}
            mydict_END[now_time.strftime("%d.%m.%Y %H:%M")] = 'Start-up time'
            mydict_END['HOSTS to restart'] = dict_hosts
    toc = time()
    print('Passed the time {}'.format(toc - tic))
    mydict_END['Passed the time'] = toc - tic
    mydict_END['done'] = '...........................................'
    return mydict_END


def shutdown_vc():
    print('Памятка бойца вц: \nvc01a-111 vc01b-112 vc01c-113 vc01d-114 vc01e-115')
    number_vc = input('\nВведите номер вц: ')
    tic = time()
    dict_hosts = {}
    now_time = datetime.datetime.now()
    for address in gen_addresses(26, number_vc):
        if pinger(address):
            dict_hosts[address] = 'online --> Command send to shutdown'
            #os.system("shutdown " "/s " "/t 0 " "/m " + address + "> nul")
            os.system("shutdown /s /t 0 /m " + address + "> nul")
            print('host {}{}'.format(address, ' online --> Command send to shutdown'))
        else:
            dict_hosts[address] = 'offline!'
            print('host {}{}'.format(address, ' offline!'))
            mydict_END = {}
            mydict_END[now_time.strftime("%d.%m.%Y %H:%M")] = 'Start-up time'
            mydict_END['HOSTS to shutdown'] = dict_hosts
    toc = time()
    print('Passed the time {}'.format(toc - tic))
    mydict_END['Passed the time'] = toc - tic
    mydict_END['done'] = '...........................................'
    return mydict_END


def ping_vc():
    print('Памятка бойца вц: \nvc01a-111 vc01b-112 vc01c-113 vc01d-114 vc01e-115')
    number_vc = input('\nВведите номер вц: ')
    tic = time()
    dict_hosts = {}
    now_time = datetime.datetime.now()
    for address in gen_addresses(26, number_vc):
        if pinger(address):
            dict_hosts[address] = 'online'
            print('host {}{}'.format(address, ' online'))
        else:
            dict_hosts[address] = 'offline!'
            print('host {}{}'.format(address, ' offline!'))
    mydict_END = {}
    mydict_END['1.Start-up time'] = now_time.strftime("%d.%m.%Y %H:%M")
    mydict_END['2.HOSTS'] = dict_hosts
    toc = time()
    print('Passed the time {}'.format(toc - tic))
    mydict_END['4.Passed the time'] = toc - tic
    mydict_END['5.done'] = '...........................................'
    return mydict_END


def del_desktop():
    print('Памятка бойца вц: \nvc01a-111 vc01b-112 vc01c-113 vc01d-114 vc01e-115')
    print('\nВыбрана функция clearing desktop.')
    number_vc = input('\nВведите номер вц: ')
    dict_list_file = {}
    for address in gen_addresses(26, number_vc):
        if pinger(address):
            print('host {}{}'.format(address, ' online'))
            try:
                #top_way = str('\\\\' + address + '\\C$\\Users\\student\\Documents\\!VM')
                top_way = str('\\\\' + address + '\\C$\\Users\\student\\Desktop')
                chmod_R(top_way)
                dict_list_file = os.listdir(top_way)
                if dict_list_file == []:
                    print('пусто')
                else:
                    print('не пусто')
                    for bit_trash in dict_list_file:
                        bit_trash_way = str(top_way + '\\' + bit_trash)
                        try:
                            if os.path.isfile(bit_trash_way):
                                print(bit_trash_way)
                                #os.chmod(bit_trash_way, stat.S_IWRITE)
                                os.remove(bit_trash_way)
                                if os.path.exists(bit_trash_way):
                                    print('{} Не удалён'.format(bit_trash_way),'\n')
                                else:
                                    print('{} Удалён'.format(bit_trash_way),'\n')
                            else:
                                print(bit_trash_way)
                                #os.chmod(bit_trash_way, stat.S_IWRITE)
                                shutil.rmtree(bit_trash_way)
                                if os.path.exists(bit_trash_way):
                                    print('{} Не удалён'.format(bit_trash_way),'\n')
                                else:
                                    print('{} Удалён'.format(bit_trash_way),'\n')
                        except OSError:
                            print(bit_trash_way)
                            print('[сброс except OSError шляпа с правами или...]')
                            #os.chmod(bit_trash_way, stat.S_IWRITE)
                    dict_list_file = os.listdir(top_way)
                    if dict_list_file == []:
                        print('пусто1э')
                    else:
                        print('непусто1э')
            except OSError:
                print('[Error]')
        else:
            print('host {}{}'.format(address, ' offline!'))


def change_of_rights():
    print('Памятка бойца вц: \nvc01a-111 vc01b-112 vc01c-113 vc01d-114 vc01e-115')
    print('\nВыбрана функция change_of_rights.')
    number_vc = input('\nВведите номер вц: ')
    dict_list_file = {}
    for address in gen_addresses(26, number_vc):
        if pinger(address):
            print('host {}{}'.format(address, ' online'))
            try:
                top_way = str('\\\\' + address + '\\C$\\test_dir')
                #top_way = str('\\\\' + address + '\\C$\\Users\\student\\Documents\\!VM')
                #top_way = str('\\\\' + address + '\\C$\\Users\\student\\Desktop')
                #chmod_R(top_way)
                dict_list_file = os.listdir(top_way)
                if dict_list_file == []:
                    print('пусто')
                else:
                    print('не пусто')
                    for bit_trash in dict_list_file:
                        bit_trash_way = str(top_way + '\\' + bit_trash)
                        try:
                            if os.path.isfile(bit_trash_way):
                                print(bit_trash_way)
                                os.chmod(bit_trash_way, stat.S_IWRITE)
                            else:
                                print(bit_trash_way)
                                os.chmod(bit_trash_way, stat.S_IWRITE)
                        except OSError:
                            print(bit_trash_way)
                            os.chmod(bit_trash_way, stat.S_IWRITE)
                    dict_list_file = os.listdir(top_way)
                    if dict_list_file == []:
                        print('пусто1э')
                    else:
                        print('непусто1э')
            except OSError:
                print('[Error]')
        else:
            print('host {}{}'.format(address, ' offline!'))


def down_func():
    try:
        print('\nВыбор действия: ')
        print('\n\tПропинговать класс ------------------------> 1')
        print('\nКопирование объектов:')
        print('\tДля копирования ФАЙЛОВ введите ------------> 2')
        print('\tДля копирования ДИРЕКТОРИИ введите --------> 3')
        print('\n!ВНИМАНИЕ, УДАЛЕНИЕ ОБЪЕКТОВ!')
        print('\tДля УДАЛЕНИЯ ФАЙЛА введите ----------------> 4')
        print('\tДля УДАЛЕНИЯ КАТАЛОГА введите -------------> 5')
        print('\n!ВНИМАНИЕ, ВЫКЛЮЧЕНИЕ И ПЕРЕЗАГРУЗКА КЛАССОВ!')
        print('\tДля ПЕРЕЗАГРУЗКИ КЛАССА введите -----------> 6')
        print('\tДля ВЫКЛЮЧЕНИЯ КЛАССА введите -------------> 7')
        print('\tДля отчистка Рабочего стола введите -------> 8')
        print('\tДля смены прав каталога--------------------> 9')

        choice_funcs = int(input('\nВведите номер соответствующий нужному действию: '))
        if choice_funcs == 1:
            yaml_dump(ping_vc())
            print('Процедура выполнена.')
        elif choice_funcs == 2:
            yaml_dump(copy_file_vc())
            print('Процедура выполнена.')
        elif choice_funcs == 3:
            yaml_dump(copy_directory_vc())
            print('Процедура выполнена.')
        elif choice_funcs == 4:
            yaml_dump(del_file_vc())
            print('Процедура выполнена.')
        elif choice_funcs == 5:
            yaml_dump(del_directory_vc())
            print('Процедура выполнена.')
            pass
        elif choice_funcs == 6:
            print('!!!ВНИМАНИЕ ВЫ СОБИРАЕТЕСЬ !!!ПЕРЕЗАГРУЗИТЬ!!! КЛАСС ВЦ!!!')
            yaml_dump(restart_vc())
            print('Процедура выполнена.')
        elif choice_funcs == 7:
            print('!!!ВНИМАНИЕ ВЫ СОБИРАЕТЕСЬ !!!ВЫКЛЮЧИТЬ!!! КЛАСС ВЦ!!!')
            yaml_dump(shutdown_vc())
            print('Процедура выполнена.')
        elif choice_funcs == 8:
            print('!!!ВНИМАНИЕ ВЫ СОБИРАЕТЕСЬ !!!ОТЧИСТИТЬ РАБОЧИЕ СТОЛЫ!!!В КЛАССЕ ВЦ!!!')
            yaml_dump(del_desktop())
            print('Процедура выполнена.')
        elif choice_funcs == 9:
            print('!!!ВНИМАНИЕ ВЫ СОБИРАЕТЕСЬ !!!СМЕНИТЬ ПРАВА НА ДИРЕКТОРИЮ!!!В КЛАССЕ ВЦ!!!')
            yaml_dump(change_of_rights())
            print('Процедура выполнена.')
        else:
            print('Вы ввели не 1,2,3,4,5,6,7,8,9.')
            print('Ошибка при выборе действия.')
    except ValueError:
        print('\nВы ввели не число.')
        print('Ошибка при выборе действия.')


if __name__ == '__main__':
    down_func()

print('\nping происходит по ip адресам, компьютеры получившие'
      '\nвременные ip не своей группы, будут идентифицированы как offline')
input('\nДля завершения нажмите Enter.')
