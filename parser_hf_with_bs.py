import urllib
import os
from bs4 import BeautifulSoup
import requests
import random
import urllib.request


def list_dir():
    if os.path.exists('pic_from_kh_f/'):
        pass
    else:
        os.makedirs('pic_from_kh_f/')


def trade_spider(max_pages):

    urls = []
    page = 1                                                              # НОМЕР СТАРТОВОЙ СТРАНИЦЫ
    while page <= max_pages:

        url = 'https://www.kharkovforum.com/showthread.php?t=4871162&page=' + str(page)

        source_code = requests.get(url).text
        soup = BeautifulSoup(source_code, "html.parser")

        for link in soup.findAll('img'):
            if link.get('src').endswith('.jpg'):
                download_web_image(link.get('src'))
                urls.append(link.get('src'))
            elif link.get('src').endswith('.jpeg'):
                urls.append(link.get('src'))
                download_web_image(link.get('src'))
        page += 1
    return urls


def download_web_image(url):
    list_dir()
    local_way = 'pic_from_kh_f/'
    file_name = os.path.basename(url).split('.')[0]
    general_way = local_way + file_name + '.jpg'
    counter = str(random.randrange(1, 1000))
    copy_general_way = local_way + file_name + '.COPY.' + counter + '.jpg'
    procedure = urllib.request.urlretrieve(url, general_way)

    if os.path.exists(general_way):
        urllib.request.urlretrieve(url, copy_general_way)
    else:
        procedure


if __name__ == '__main__':
    trade_spider(2)

# trade_spider(1)                                                          # ТОЛЬКО СТАРТОВЮУ СТРАНИЦУ
# trade_spider(2)                                                         # ОТ СТАРТОВОЙ СТРАНИЦЫ И НА 20 ВГЛУБЬ
# for i in trade_spider(50):
#     print(i)
