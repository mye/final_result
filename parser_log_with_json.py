import os
import io
import json
import codecs


''' Recoding to UTF-8 '''
os.system('find . -name "*.txt" -exec enconv -L russian -x UTF-8 {} \;')

topway = os.getcwd()


def filenames():
    dict_list_file={}
    dict_list_file = os.listdir(os.getcwd())
    return dict_list_file


def touches(dir_with_files):
    dict_touches = {}
    dict_touches_end = {}
    for filename in dir_with_files:
        chek_target = topway + '/' + filename
        path_to_file, suffiks_to_file = os.path.splitext(chek_target)
        if suffiks_to_file == '.txt':
            f = (topway + '/' + filename)
            fn = found(f)
            fncod = encoding_of_files(f)
            print('{} -> {}'.format(filename, fncod))
            dict_touches[filename] = fn
            dict_touches_end = dict_touches
    return dict_touches



def found(target):
    dict_touches_test = {}
    dict_touches_test_1 = {}
    words = ('Computer Name', 'CPUID CPU Name', 'Video Adapter', 'Processors', 'LGA', 'Disk Drive', 'System Memory', 'Motherboard Name',)
    for number1, word in enumerate(words):
        # with io.open(target, encoding='utf-8') as file:
        with io.open(target) as file:
            for line in file:
                if word in line:
                    line = (' '.join(line.split()))
                    dict_touches_test[number1] = line
                    break
                else:
                    dict_touches_test[number1] = word + ' NOT FOUND'
    dict_touches_test_1 = dict_touches_test
    return dict_touches_test


def json_dump(mydict_END_local):
        with codecs.open('log.txt', "wt") as f:
            json.dump(mydict_END_local, f, indent=1, ensure_ascii=0)


def encoding_of_files(file):
    encoding = [
        'utf-8',
        'cp500',
        'utf-16',
        'GBK',
        'windows-1251',
        'ASCII',
        'US-ASCII',
        'Big5'
    ]
    correct_encoding = ''
    for enc in encoding:
        try:
            open(file, encoding=enc).read()
        except (UnicodeDecodeError, LookupError):
            pass
        else:
            correct_encoding = enc
            # print('{}{}'.format('Done! ', correct_encoding))
            break
    return enc


if __name__ == '__main__':
    json_dump(touches(sorted(filenames())))

