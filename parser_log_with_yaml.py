import os
import io
import yaml
import re


''' Recoding to UTF-8 '''
os.system('find . -name "*.txt" -exec enconv -L russian -x UTF-8 {} \;')

topway = os.getcwd()


def filenames():
    dict_list_file={}
    dict_list_file = os.listdir(os.getcwd())
    return dict_list_file


def touches(dir_with_files):
    dict_touches = {}
    dict_touches_end = {}
    for filename in dir_with_files:
        chek_target = topway + '/' + filename
        path_to_file, suffiks_to_file = os.path.splitext(chek_target)
        if suffiks_to_file == '.txt':
            f = (topway + '/' + filename)
            # print(filename)
            fn = found(f)
            dict_touches[filename] = fn
    dict_touches_end = dict_touches
    return dict_touches


def found(target):
    dict_touches_test = {}
    dict_touches_test_1 = {}
    words = ('Computer Name', 'CPUID CPU Name', 'Video Adapter', 'Processors', 'LGA', 'Disk Drive', 'System Memory', 'Motherboard Name',)
    # words = ('Computer Name',)
    # for word in words:
    for number1, word in enumerate(words):
        # with io.open(target, encoding='utf-8') as file:
        with io.open(target) as file:
            for line in file:
                if word in line:
                    line = (' '.join(line.split()))
                    # print(line, end='')
                    # print('\n', line, end='')
                    dict_touches_test[number1] = line
                    break
    dict_touches_test_1 = dict_touches_test
    return dict_touches_test


def logdir():
    if os.path.exists('log/copy_log_vc.yaml'):
        my_file = open("log/copy_log_vc.yaml", 'w')
        my_file.close()
    else:
        if os.path.exists('log/'):
            my_file = open("log/copy_log_vc.yaml", 'w')
            my_file.close()
        else:
            os.makedirs('log')
            my_file = open("log/copy_log_vc.yaml", 'w')
            my_file.close()


def yaml_dump(mydict_END_local):
    logdir()
    with open('log/copy_log_vc.yaml', 'w', encoding='utf-8') as fout:
        yaml.dump(mydict_END_local, fout, indent=4,
                  default_flow_style=False,
                  allow_unicode=True)


if __name__ == '__main__':
    yaml_dump(touches(sorted(filenames())))
