import time
import multiprocessing
import os
import sys
import concurrent.futures


start = time.perf_counter()

def ping_host(ip):
    list_ip = []
    if sys.platform == "linux":
        if os.system('ping -c 1 -W 1 ' + str(ip) + '> /dev/null') != 0:
            print(f'{ip} is offline.')
            list_ip.append(f'{ip}')
            # return (f'{ip} is online.')
    else:
        if os.system('ping -n 1 -w 1000 ' + str(ip) + '> /null') != 0:
            print(f'{ip} is offline.')
            # return (f'{ip} is online.')
    return list_ip


def main():
    number_vc = input('\nВведите номер вц: ')
    processes = []

    for work_spot in range(25):
        p = multiprocessing.Process(target=ping_host, args=[f'192.168.{number_vc}.{work_spot + 1}'])
        p.start()
        processes.append(p)

    for process in processes:
        process.join()
        # lkr = []
        # po = process.join()
        # lkr.append(po)
        # sorted(po)
        # print(type(po))

        # print(process.join())


if __name__ == '__main__':
    main()


finish = time.perf_counter()
print(f'Finish in {round(finish-start, 2)} second(s)')
